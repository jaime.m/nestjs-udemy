import { Controller, Get, Post, Body, Param, Delete, Patch, Query, UsePipes, ValidationPipe, NotFoundException } from '@nestjs/common';
import { TasksService } from './tasks.service';
import { Task, TaskStatus } from './task.model';
import { CreateTaskDto } from './dto/create-task-dto';
import { stat } from 'fs';
import { GetTaskFilterDto } from './dto/get-tas.filter-dto';
import { query } from 'express';
import { TaskStatusValidationPipe } from './pipes/task-status-validation-pipe';

@Controller('tasks')
export class TasksController {
constructor(private taskService:TasksService){
    
}
    @Get()
    getTasks(@Query(ValidationPipe) filterDto:GetTaskFilterDto):Task[]{
        console.log(filterDto);
        if(Object.keys(filterDto).length){
            return this.taskService.getTaskWithFilters(filterDto)
        }else{
            return this.taskService.getAllTask()
        }
    }


    @Get('/:id')
    getTasksById(@Param('id') id:string):Task{
        console.log(id);
        const found =this.taskService.getTaskById(id)

        return found
    }

    @Patch('/:id/status')
    updateTasksById(
        @Param('id') id:string,@Body('status',TaskStatusValidationPipe) status:TaskStatus):Task{

        return this.taskService.updateTasksById(id,status)
    }

    @Delete('/:id')
    deleteTasksById(@Param('id') id:string){
        console.log(id);
        this.taskService.deleteTaskById(id)

    }

    @Post()
    @UsePipes(ValidationPipe)
    createTask(@Body() createTaskDto:CreateTaskDto):Task{
    console.log(createTaskDto.description);
    return this.taskService.createTask(createTaskDto)
    }

}
