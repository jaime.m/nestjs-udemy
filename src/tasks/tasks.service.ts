import { Injectable, NotFoundException } from '@nestjs/common';
import { Task, TaskStatus } from './task.model';
import { CreateTaskDto } from './dto/create-task-dto';
import { GetTaskFilterDto } from './dto/get-tas.filter-dto';

@Injectable()
export class TasksService {
private tasks:Task[]=[]

    getAllTask():Task[]{
        return this.tasks
    }

    getTaskWithFilters(filterDto:GetTaskFilterDto):Task[]{
    const {status,search}=filterDto    
    let task = this.getAllTask()
    if(status){
        task=task.filter(x=>x.status===status)
    }
    if(search)
        task=task.filter(x=>
            x.title.includes(search)||
            x.description.includes(search))

        return task
    }

    getTaskById(id):Task{
        let found =this.tasks.find(x=>x.id==id)
        if(!found){
            throw new NotFoundException(`Tassk with id ${id} was not found`)
        }
        return found
    }

    deleteTaskById(id):void{
        
        let found =this.getTaskById(id)
        if(!found){
            throw new NotFoundException(`Tassk with id ${id} was not found`)
        }

        this.tasks = this.tasks.filter(task=>task.id!==found.id);
        
    }

    updateTasksById(id:string, status:TaskStatus):Task{

        let task= this.tasks.find(x=>x.id==id)
        task.status=status

         return task

    }

    createTask(createTaskDto:CreateTaskDto):Task{
        const {title,description}=createTaskDto
        const task:Task ={
            id:"13",
            title,description,status:TaskStatus.OPEN
        }
        this.tasks.push(task)
        return task
    }
}
